# Artifical Neural Network

'''
Training the ANN with Stochastic Gradient Decent

Step1: Randomly initalise the weights to small numbers close to 0 (but not 0)

Step2: Input the first observation of your dataset in the input layer,
 each feature in one input node.

Step3: Forward-Propagation: from left to right, the neurons are activated in a way
 that impact each neuron's activation is limited by the weights. Propagate the
  activations until getting the predicted result y.

Step4: Compare the predicted result to the actual result. Measure the generated error.

Step5: Back-Propagation: from right to left, the error is back-propagated.
 Update the weights according to how much they are responsible for the error.
  The learning rate decides by how much we update the weights.

Step6: Repeat Steps 1 to 5 and update tht weights after each observation
 (Reinforcement Learning). Or:
 Repeat Steps 1 to 5 but update the weights only after a batch of
  observations (Batch Learning).

Step7: When the whole training set passed through the ANN, that makes an epoch.
 Redo more epochs.

rectifier activation function used, best option for hidden layers
sigmoid function used for probability for output layer

'''

# part 1 - Data Preprocessing

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values

# Encoding categorical data - turning words into numerical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
label_encoder_X_1 = LabelEncoder()
X[:, 1] = label_encoder_X_1.fit_transform(X[:, 1])
label_encoder_X_2 = LabelEncoder()
X[:, 2] = label_encoder_X_2.fit_transform(X[:, 2])

# create dummy variables for the country variables as there are more then 2.
onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
# now remove one country so that you dont have the dummy variable trap
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)


# Part 2 - now lets make the ANN!

# Importing the Keras libraries and packages
import keras
from keras.models import Sequential
from keras.layers import Dense

# initialising the ANN
classifier = Sequential()

# Adding the input layer and the first hidden layer - updated for Keras 2 API
classifier.add(Dense(6, kernel_initializer='uniform', activation='relu', input_dim=11))

# Adding the Second hidden layer
classifier.add(Dense(6, kernel_initializer='uniform', activation='relu'))

# adding the output layer
classifier.add(Dense(1, kernel_initializer='uniform', activation='sigmoid'))
# for non binary outputs ie 3+ use the 'softmax' activation method = sigmoid but for more catogories.

# Compiling the ANN
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Fitting the ANN to the Training set
classifier.fit(X_train, y_train, batch_size=10, epochs=100)

# Part 3 - Making the predictions and evaluating the model

# Predicting the Test set results
y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)
# Making the Confusion Matrix - Evaluation
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

