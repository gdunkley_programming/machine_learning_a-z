#Importing the Libraries
import numpy as pd
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans

# Importing the mall dataset with pandas
dataset = pd.read_csv('Section 24 - K-Means Clustering/Mall_Customers.csv')
X_dataset = dataset.iloc[:, [3,4]].values

# using the elbow method to find the optiomal number of cluster
# from sklearn.cluster import KMeans
wcss = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
    kmeans.fit(X_dataset)
    wcss.append(kmeans.inertia_)

plt.plot(range(1, 11), wcss)
plt.title('The Elbow Method')
plt.xlabel('Number of Clusters')
plt.ylabel('WCSS')
plt.show()


# Applying KMeans to the dataset
kmeans = KMeans(n_clusters=5, init='k-means++', max_iter=300, n_init=10, random_state=0)
y_kmeans = kmeans.fit_predict(X_dataset)


# Visualising the clusters
plt.scatter(X_dataset[y_kmeans == 0, 0], X_dataset[y_kmeans == 0, 1], s=10, c='red', label='Careful')
plt.scatter(X_dataset[y_kmeans == 1, 0], X_dataset[y_kmeans == 1, 1], s=10, c='blue', label='Standard')
plt.scatter(X_dataset[y_kmeans == 2, 0], X_dataset[y_kmeans == 2, 1], s=10, c='green', label='Target')
plt.scatter(X_dataset[y_kmeans == 3, 0], X_dataset[y_kmeans == 3, 1], s=10, c='cyan', label='Careless')
plt.scatter(X_dataset[y_kmeans == 4, 0], X_dataset[y_kmeans == 4, 1], s=10, c='magenta', label='Sensible')
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=30, c='yellow', label='Centroids')
plt.title('Clusters of Shopping Clients')
plt.xlabel('Annual Income (K$)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()


