# Hierarchical Clustering

# Importing the Librarys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the mall dataset with pandas
dataset = pd.read_csv('Section 25 - Hierarchical Clustering/Mall_customers.csv')
X_hc = dataset.iloc[:, [3, 4]].values

# Using the dendrogram to find the optimal number of clusters
import scipy.cluster.hierarchy as sch
dendrogram = sch.dendrogram(sch.linkage(X_hc, method='ward'))
plt.title('Dendrogram')
plt.xlabel('Customers')
plt.ylabel('Euclidean Distance')
plt.show()

# Fitting hierarchical Clustering to the mall dataset
from sklearn.cluster import AgglomerativeClustering
hc = AgglomerativeClustering(n_clusters=5, affinity='euclidean', linkage='ward')
y_hc = hc.fit_predict(X_hc)

# Visualising the Hierarchical Clusters

plt.scatter(X_hc[y_hc == 0, 0], X_hc[y_hc == 0, 1], s=10, c='red', label='Conservative')
plt.scatter(X_hc[y_hc == 1, 0], X_hc[y_hc == 1, 1], s=10, c='blue', label='Standard')
plt.scatter(X_hc[y_hc == 2, 0], X_hc[y_hc == 2, 1], s=10, c='green', label='Target')
plt.scatter(X_hc[y_hc == 3, 0], X_hc[y_hc == 3, 1], s=10, c='cyan', label='Irresponsible')
plt.scatter(X_hc[y_hc == 4, 0], X_hc[y_hc == 4, 1], s=10, c='magenta', label='Sensible')
plt.title('Clusters of customers')
plt.xlabel('Annual Income (k$)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()