# Polynomial Regression

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('CapFinancialData1.csv')
X = dataset.iloc[:, 0:1].values
y = dataset.iloc[:, 2].values

# Splitting the dataset into the Training set and Test set
'''from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)'''

# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)"""

# Fitting Linear Regression to the dataset
from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)

# Fitting Polynomial Regression to the dataset
from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree = 4)
X_poly = poly_reg.fit_transform(X)
lin_reg2 = LinearRegression()
lin_reg2.fit(X_poly, y)

# Visualising the Linear Regression results
plt.scatter(X, y, color = 'red')
plt.plot(X, lin_reg.predict(X), color = 'yellow')
plt.title('IAG Monthly Polymonimal Regression')
plt.xlabel('Month Number (from Contract Start')
plt.ylabel('Monthly Cost')
plt.show()

# Visualising the Polynomial Regression results
X_grid = np.arange(min(X), max(X), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X, y, color = 'red')
plt.plot(X_grid, lin_reg2.predict(poly_reg.fit_transform(X_grid)), color = 'blue')
plt.title('IAG Monthly Polymonimal Regression')
plt.xlabel('Month Number (from Contract Start)')
plt.ylabel('Monthly Cost')
plt.show()

# Predicting a new result with Linear Regression
lin_reg.predict(55)

# Predicting a new result with Polynomial Regression
lin_reg2.predict(poly_reg.fit_transform(55))
lin_reg2.predict(poly_reg.fit_transform(56))

# Predictiing the next year (12) monthly chargeback results with Polynomial Regression
count = 3
mth_num = 56
while count < 12:
    print(lin_reg2.predict(poly_reg.fit_transform(mth_num)), '-',  count)
    mth_num = mth_num + 1
    count += 1

