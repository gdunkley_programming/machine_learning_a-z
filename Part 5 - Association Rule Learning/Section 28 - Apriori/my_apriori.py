# Apriori

# Importing the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, 'Section 28 - Apriori')  # set the directory where the apyori class exists allowing import.
from apyori import apriori


# Importing the dataset
dataset = pd.read_csv('Section 28 - Apriori/Market_Basket_Optimisation.csv', header=None)
transactions = []
for val in range(0,7501):
    transactions.append([str(dataset.values[val, j]) for j in range(0, 20)])

# Long code version of above for loop
# trans = []
# for val1 in range(0, 7501):
#     temp_list = []
#     for val2 in range(0, 20):
#         temp_list.append(str(dataset.values[val1, val2]))
#     trans.append(temp_list)

rules = apriori(transactions, min_support=0.003, min_confidence=0.2, min_lift=3, min_length=2)

# Visualising the results
results = list(rules)
